package exercici9Joc;

import java.util.ArrayList;
import java.util.List;

public class Baralla {
	List<Carta> lista = new ArrayList<Carta>();

	public Baralla() {
		TYPE tipo = null;
		Carta car;
		for (int j = 0; j < 4; j++) {
			if (j == 0)
				tipo = TYPE.OROS;
			if (j == 1)
				tipo = TYPE.COPAS;
			if (j == 2)
				tipo = TYPE.ESPADAS;
			if (j == 3)
				tipo = TYPE.BASTOS;
			for (int i = 1; i < 13; i++) {

				 car = new Carta(i, tipo);
				 System.out.println(car.toString());
				lista.add(car);
			}
		}
		this.lista=lista;
	}
	
	public void mezcla() {
		
	}

	@Override
	public String toString() {
		return "Baralla [lista=" + lista + "]";
	}
	
}
