package exercici9Joc;

public class Carta {

	private TYPE tipo;
	private int num;

	public Carta(int num, TYPE tipo) {
		try {
			if (num > 12 || num < 1) {
				throw new IllegalArgumentException("El valor debe ser entre 1 y 12");
			}else this.num=num;
			this.tipo=tipo;
		} catch (IllegalArgumentException e) {
			System.err.println(e);// tambi�n podr�a poner e.getMessage()
		}
	}

	public TYPE getTipo() {
		return tipo;
	}

	public int getNum() {
		return num;
	}

	@Override
	public String toString() {
		return "Carta [tipo=" + tipo + ", num=" + num + "]";
	}

	

}
