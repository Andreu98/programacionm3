package exercici9Joc;

public enum TYPE {
	OROS(0,'O'), COPAS(1,'C'), ESPADAS(2,'E'), BASTOS(3,'B');

	private final int valor;
	private char c;

	TYPE(int valor,char c) {
		this.valor = valor;
		this.c=c;
	}

	public int getValor() {
		return valor;
	}
	public int getc() {
		return c;
	}
	
	
	
}
