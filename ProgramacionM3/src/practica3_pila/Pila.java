package practica3_pila;

import java.util.ArrayDeque;
import java.util.Stack;

public class Pila {
	
	//las dos funcionan ;D
	private static ArrayDeque<Character> pila = new ArrayDeque<Character>();
	//private static Stack<Character> pila = new Stack<Character>();
	public static boolean Comprovacion(String formula) {

		//pasamos la formula a un array de chars
		char[] caracteres = formula.toCharArray();
		boolean comprovador = true;

		for (char c : caracteres) {
			//abertura
			if (c == '(') {
				pila.push(c);
			}
			if (c == '[') {
				pila.push(c);
			}
			if (c == '{') {
				pila.push(c);
			}
			
			//cierre
			if (c == ')') {
				if (pila.isEmpty()) {
					comprovador = false;
				} else if (pila.pop() != '(') {
					comprovador = false;
				}
			}
			if (c == ']') {
				if (pila.isEmpty()) {
					comprovador = false;
				} else if (pila.pop() != '[') {
					comprovador = false;
				}
			}
			if (c == '}') {
				if (pila.isEmpty()) {
					comprovador = false;
				} else if (pila.pop() != '{') {
					comprovador = false;
				}
			}
		}

		if (!pila.isEmpty())
			comprovador = false;
		return comprovador;
	}

}
