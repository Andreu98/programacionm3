package practica2m3uf5;

public interface SerieE2 {
	
	
	/**
	 * marca donde empieza la serie
	 * @param start
	 */
	public void setStart(int start);
		
	
	/**
	 * da el siguiete numero de la serie
	 * @return
	 */
	public int getNext();
	/**
	 * reinicia la serie a donde marque la variable start
	 */
	public void restart();
	
	public void ciclarx20();

}
