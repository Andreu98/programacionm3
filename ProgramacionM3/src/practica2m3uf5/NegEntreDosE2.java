package practica2m3uf5;

public class NegEntreDosE2 implements SerieE2 {
	private int start;
	private int value;
	
	
	@Override
	public void setStart(int start) {
		this.start=start;
		this.value=start;

	}

	@Override
	public int getNext() {
		value=(value/2)*-1;
		return value;
	}

	@Override
	public void restart() {
		value=start;

	}

	@Override
	public void ciclarx20() {
		
		for (int i = 0; i < 20; i++) {
			System.out.println(getNext());
		}
		
	}

}
