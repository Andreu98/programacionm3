package practica2m3uf5;

public class EstudiantE4 implements Comparable<EstudiantE4>{

	private String nom;
	private int edad;
	private int curs;

	public EstudiantE4(String nom, int edad, int curs) {

		this.nom = nom;
		this.edad = edad;

		if (curs > 5) {
			curs = 5;
		}
		if (curs < 1) {
			curs = 1;
		}
		this.curs = curs;
	}

	public String getNom() {
		return nom;
	}

	public int getEdad() {
		return edad;
	}

	public int getCurs() {
		return curs;
	}

	@Override
	public int compareTo(EstudiantE4 o) {
		return this.getNom().compareTo(o.getNom());
		
	}

}
