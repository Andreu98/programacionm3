package practica2m3uf5;

import java.util.ArrayList;

public class MainEstudiantes {

	public static void main(String[] args) {
		
		
		EstudiantE4 estudiante0 = new EstudiantE4("Paco",15,3); 
		EstudiantE4 estudiante1 = new EstudiantE4("Clemente",17,5);
		EstudiantE4 estudiante2 = new EstudiantE4("Andrew",15,2);
		EstudiantE4 estudiante3 = new EstudiantE4("Davilillo",14,1);
		EstudiantE4 estudiante4 = new EstudiantE4("Dani",18,4);
		
		ArrayList<EstudiantE4> ls = new ArrayList<EstudiantE4>();
		ls.add(estudiante4);
		ls.add(estudiante3);
		ls.add(estudiante2);
		ls.add(estudiante1);
		ls.add(estudiante0);
	}

}
