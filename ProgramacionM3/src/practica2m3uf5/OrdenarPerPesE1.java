package practica2m3uf5;

import java.util.Comparator;

public class OrdenarPerPesE1 implements Comparator<PersonaE1> {

	@Override
	public int compare(PersonaE1 o1, PersonaE1 o2) {
		return o1.getPes()-o2.getPes();
	}
	

}
