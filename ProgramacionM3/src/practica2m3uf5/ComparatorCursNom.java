package practica2m3uf5;

import java.util.Comparator;

public class ComparatorCursNom implements Comparator<EstudiantE4>{

	@Override
	public int compare(EstudiantE4 o1, EstudiantE4 o2) {
		if(o1.getCurs()-o2.getCurs()==0) {
			return o1.compareTo(o2);
		}
		else return o1.getCurs()-o2.getCurs();
		
	}

}
