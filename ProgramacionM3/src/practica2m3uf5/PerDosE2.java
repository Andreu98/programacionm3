package practica2m3uf5;



public class PerDosE2 implements SerieE2 {
	private int start;
	private int value;
	
	

	/**
	 * constructor
	 */
	public PerDosE2() {
		this.start=0;
		this.value=0;
	}
	/**
	 * indica el comienzo de la serie
	 */
	@Override
	public void setStart(int start) {
		this.start=start;
		this.value=start;

	}
	/**
	 * da el siguiente numero de la serie
	 */
	@Override
	public int getNext() {
		value+=2;
		return value;
	}
	/**
	 * asigna start a value para regresar a la posicion de setStart anterior.
	 */
	@Override
	public void restart() {
		value=start;
	}
	@Override
	public void ciclarx20() {
		
		for (int i = 0; i < 20; i++) {
			System.out.println(getNext());
		}
		
	}

}
