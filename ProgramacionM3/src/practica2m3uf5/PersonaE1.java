package practica2m3uf5;


public class PersonaE1 implements Comparable<PersonaE1>{
	
	private int pes;
	private int edat;
	private int al�ada;
	

	public PersonaE1(int edat, int pes, int al�ada) {
		super();
		this.pes = pes;
		this.edat = edat;
		this.al�ada = al�ada;
	}

	public int getPes() {
		return pes;
	}

	public void setPes(int pes) {
		this.pes = pes;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public int getAl�ada() {
		return al�ada;
	}

	public void setAl�ada(int al�ada) {
		this.al�ada = al�ada;
	}

	
	@Override
	public int compareTo(PersonaE1 o) {
	
		if(this.al�ada>o.al�ada) {
			return 1;
		}else if (this.al�ada<o.al�ada) {
			return -1;
		}else return 0;
		
	}

	

	
	
	
	

	

}
