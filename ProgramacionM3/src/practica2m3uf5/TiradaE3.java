package practica2m3uf5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class TiradaE3 implements Comparable<TiradaE3>, Cloneable {

	private static Random r = new Random();
	private ArrayList<Integer> dados = new ArrayList<Integer>();
	private int atc;
	private int def;

	public ArrayList<Integer> getDados() {
		return dados;
	}

	public int getAtc() {
		return atc;
	}

	public int getDef() {
		return def;
	}

	/**
	 * Constructor al que le pasas el arrayList con los valores de los dados ya
	 * puestos
	 * 
	 * @param dados
	 * @param atc
	 * @param def
	 */
	public TiradaE3(ArrayList<Integer> dados, int atc, int def) {
		this.atc = atc;
		this.def = def;
		this.dados = dados;
		Collections.sort(this.dados);
		Collections.reverse(this.dados);
	}

	/**
	 * Constructor al que le pasas el numero de dados, este constructor te genera
	 * los numeros de cada dado el numero por defecto de caras es 6
	 * 
	 * @param dados
	 * @param atc
	 * @param def
	 */
	public TiradaE3(int numeroDados, int atc, int def) {
		this.atc = atc;
		this.def = def;
		for (int i = 0; i < numeroDados; i++) {
			this.dados.add(r.nextInt(6) + 1);
		}
		Collections.sort(this.dados);
		Collections.reverse(this.dados);
	}

	/**
	 * Costructor al que le pasas el numero de dados y el numero de caras, te genera
	 * el valor en la tirada que va desde 1 a numeroCaras
	 * 
	 * @param numeroDados
	 * @param numeroCaras
	 * @param atc
	 * @param def
	 */
	public TiradaE3(int numeroDados, int numeroCaras, int atc, int def) {
		this.atc = atc;
		this.def = def;
		for (int i = 0; i < numeroDados; i++) {
			this.dados.add(r.nextInt(numeroCaras) + 1);
		}
		Collections.sort(this.dados);
		Collections.reverse(this.dados);
	}

	@Override
	public int compareTo(TiradaE3 otro) {
		int victorias = 0;
		int otroVictoria= 0;
		int ataque;
		int defensa;
		
		//si el arraylist de este objeto es mas peque�o que el del otro
		if (otro.getDados().size() > this.dados.size()) {
			
			for (int i = 0; i < dados.size(); i++) {
				ataque= this.getDados().get(i)+this.atc;
				defensa= otro.getDados().get(i)+otro.def;
				if(ataque>defensa) {
					victorias++;
				}
			}
			
			for (int i = 0; i < dados.size(); i++) {
				ataque= otro.getDados().get(i)+otro.atc;
				defensa=this.getDados().get(i)+this.def; 
				if(ataque>defensa) {
					otroVictoria++;
				}
			}
		}
		//si el arraylist de este objeto es igual o mas grande que el otro
		if(otro.getDados().size() <= this.dados.size()) {
			
			for (int i = 0; i < otro.getDados().size(); i++) {
				ataque= this.getDados().get(i)+this.atc;
				defensa= otro.getDados().get(i)+otro.def;
				if(ataque>defensa) {
					victorias++;
				}
			}
			
			for (int i = 0; i < otro.getDados().size(); i++) {
				ataque= otro.getDados().get(i)+otro.atc;
				defensa=this.getDados().get(i)+this.def; 
				if(ataque>defensa) {
					otroVictoria++;
				}
			}
		}
		
		
		return victorias-otroVictoria;
	}
	@Override
	public TiradaE3 clone() {
		TiradaE3 t = null;
		//me obliga a hacer el try and catch
		try {
			t=(TiradaE3) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return t;
		
	}

	@Override
	public String toString() {
		return "TiradaE3 [dados=" + dados + ", atc=" + atc + ", def=" + def + "]";
	}
	

}
