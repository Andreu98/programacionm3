package practica2m3uf5;

import java.util.Comparator;

public class ComparatorEdat implements Comparator<EstudiantE4> {

	@Override
	public int compare(EstudiantE4 est0, EstudiantE4 est1) {
		
		return est0.getEdad()-est1.getEdad();
	}
}
