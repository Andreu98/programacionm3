package practica2m3uf5;

public class PruebasTiradaE3 {

	public static void main(String[] args) {
		
		//jugador 1
		TiradaE3 j1 = new TiradaE3(3,3,1);
		//jugador 2 
		TiradaE3 j2 = new TiradaE3(4,2,2);
		
		System.out.println(j1.toString());
		System.out.println(j2.toString());
		System.out.println(j1.compareTo(j2));

		TiradaE3 j3 = j1.clone();
		System.out.println(j3.toString());
	}

}
