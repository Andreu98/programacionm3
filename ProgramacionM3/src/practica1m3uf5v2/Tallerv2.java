package practica1m3uf5v2;

import java.util.Random;

public class Tallerv2 {
	private static final int SIZE = 5;
	MarkJavav2[][] robots = new MarkJavav2[SIZE][SIZE];
	Random random = new Random();
	/**
	 * explore the array of robots an execute the resolveMove function ( if it can be executed)
	 */
	private void turn() {

		for (int f = 0; f < SIZE; f++) {
			for (int c = 0; c < SIZE; c++) {

				if (robots[f][c] == null) {
				} else {
					if (comproveDestructorv2(robots[f][c])) {
						System.out.println("turno destructor");
						if (((Destructorv2) robots[f][c]).decideToMove()) {

							resolveMove(robots[f][c], f, c);
						}
					}
					if (comproveReplicantv2(robots[f][c])) {
						System.out.println("turno replicante");
						if (((Replicantv2) robots[f][c]).decideToMove()) {

							resolveMove(robots[f][c], f, c);
						}
					}
					if (comproveMecanicv2(robots[f][c])) {
						System.out.println("turno mecanico");
						if (((Mecanicv2) robots[f][c]).decideToMove()) {

							resolveMove(robots[f][c], f, c);
						}
					}

				}
			}
		}
		for (int f = 0; f < SIZE; f++) {
			for (int c = 0; c < SIZE; c++) {
				if (robots[f][c] != null) {
					robots[f][c].setHasMove(false);
				}
			}
		}
		showTaller();
	}

	/**
	 * this function executes the method interact, of he robot, depending on the type
	 * @param robotToMove
	 * @param x
	 * @param y
	 */
	private void resolveMove(MarkJavav2 robotToMove, int x, int y) {
		int direction = random.nextInt(4);
		// right
		if (direction == 0) {
			if (y == 4) {
				// this is to predict the indexoutofbounds
			} else {
				// checking for a possible collision with another robot
				if (robots[x][y + 1] != null) {
					if (comproveDestructorv2(robots[x][y])) {
						((Destructorv2) robots[x][y]).interact(robots[x][y + 1]);
					}
					if (comproveReplicantv2(robots[x][y])) {
						((Replicantv2) robots[x][y]).interact(robots[x][y + 1]);
					}
					if (comproveMecanicv2(robots[x][y])) {
						((Mecanicv2) robots[x][y]).interact(robots[x][y + 1]);
					}
				} else {
					// posible problema con la memoria, revisar.
					robots[x][y].energyWasted(1);
					if (!comproveReplicantv2(robots[x][y])) {
						robots[x][y + 1] = robots[x][y];
						robots[x][y] = null;
					} else {
						robots[x][y + 1] = robots[x][y];
						// robots[x][y] = ((Replicantv2) robots[x][y + 1]).buildARobot();
						createNewRobot(((Replicantv2) robots[x][y + 1]), x, y);
					}
				}
			}
		}
		// left
		if (direction == 2) {
			if (y == 0) {
				// this is to predict the indexoutofbounds
			} else {
				// checking for a possible collision with another robot
				if (robots[x][y - 1] != null) {
					if (comproveDestructorv2(robots[x][y])) {
						((Destructorv2) robots[x][y]).interact(robots[x][y - 1]);
					}
					if (comproveReplicantv2(robots[x][y])) {
						((Replicantv2) robots[x][y]).interact(robots[x][y - 1]);
					}
					if (comproveMecanicv2(robots[x][y])) {
						((Mecanicv2) robots[x][y]).interact(robots[x][y - 1]);
					}
				} else {
					// posible problema con la memoria, revisar.
					robots[x][y].energyWasted(1);
					if (!comproveReplicantv2(robots[x][y])) {
						robots[x][y - 1] = robots[x][y];
						robots[x][y] = null;
					} else {
						robots[x][y - 1] = robots[x][y];
						// robots[x][y] = ((Replicantv2) robots[x][y - 1]).buildARobot();
						createNewRobot(((Replicantv2) robots[x][y - 1]), x, y);
					}

				}
			}
		}
		// up
		if (direction == 3) {
			if (x == 0) {
				// this is to predict the indexoutofbounds
			} else {
				// checking for a possible collision with another robot
				if (robots[x - 1][y] != null) {
					if (comproveDestructorv2(robots[x][y])) {
						((Destructorv2) robots[x][y]).interact(robots[x - 1][y]);
					}
					if (comproveReplicantv2(robots[x][y])) {
						((Replicantv2) robots[x][y]).interact(robots[x - 1][y]);
					}
					if (comproveMecanicv2(robots[x][y])) {
						((Mecanicv2) robots[x][y]).interact(robots[x - 1][y]);
					}
				} else {
					// posible problema con la memoria, revisar.
					robots[x][y].energyWasted(1);
					if (!comproveReplicantv2(robots[x][y])) {
						robots[x - 1][y] = robots[x][y];
						robots[x][y] = null;
					} else {
						robots[x - 1][y] = robots[x][y];
						// robots[x][y] = ((Replicantv2) robots[x - 1][y]).buildARobot();
						createNewRobot(((Replicantv2) robots[x - 1][y]), x, y);
					}
				}
			}
		}
		// down
		if (direction == 1) {
			if (x == 4) {
				// this is to predict the indexoutofbounds
			} else {
				// checking for a possible collision with another robot
				if (robots[x + 1][y] != null) {
					if (comproveDestructorv2(robots[x][y])) {
						((Destructorv2) robots[x][y]).interact(robots[x + 1][y]);
					}
					if (comproveReplicantv2(robots[x][y])) {
						((Replicantv2) robots[x][y]).interact(robots[x + 1][y]);
					}
					if (comproveMecanicv2(robots[x][y])) {
						((Mecanicv2) robots[x][y]).interact(robots[x + 1][y]);
					}
				} else {
					// posible problema con la memoria, revisar.
					robots[x][y].energyWasted(1);
					if (!comproveReplicantv2(robots[x][y])) {
						robots[x + 1][y] = robots[x][y];
						robots[x][y] = null;
					} else {
						robots[x + 1][y] = robots[x][y];
						// robots[x][y] = ((Replicantv2) robots[x + 1][y]).buildARobot();
						System.out.println("entra aqui");
						createNewRobot(((Replicantv2) robots[x + 1][y]), x, y);
					}
				}
			}
		}

	}
	/**
	 * show the "taller"
	 */
	private void showTaller() {
		System.out.println("----------");
		for (int f = 0; f < SIZE; f++) {
			for (int c = 0; c < SIZE; c++) {
				if (robots[f][c] == null) {
					System.out.print("- ");
				} else {
					if (comproveDestructorv2(robots[f][c])) {
						System.out.print("D ");
					}
					if (comproveReplicantv2(robots[f][c])) {
						System.out.print("R ");
					}
					if (comproveMecanicv2(robots[f][c])) {
						System.out.print("M ");
					}
				}
			}
			System.out.println();
		}
		System.out.println("----------");
	}
	/**
	 * creates the "taller"
	 */
	public void createTaller() {
		int d = 1, m = 1, r = 1;

		while (d > 0) {
			int f = random.nextInt(5);
			int c = random.nextInt(5);
			if (robots[f][c] == null) {
				Destructorv2 destructor = new Destructorv2();
				robots[f][c] = destructor;
				d--;
			}
		}
		while (m > 0) {
			int f = random.nextInt(5);
			int c = random.nextInt(5);
			if (robots[f][c] == null) {
				Mecanicv2 mecanic = new Mecanicv2();
				robots[f][c] = mecanic;
				m--;
			}
		}
		while (r > 0) {
			int f = random.nextInt(5);
			int c = random.nextInt(5);
			if (robots[f][c] == null) {
				Replicantv2 replicant = new Replicantv2();
				robots[f][c] = replicant;
				r--;
			}
		}
		showTaller();
		showEnergy();
		doTurns(20);

	}
	/**
	 * this method is called by resolveMode in case the robot that moves is of replicant class
	 * @param robotThatCreates
	 * @param x
	 * @param y
	 */
	private void createNewRobot(Replicantv2 robotThatCreates, int x, int y) {

		robots[x][y] = robotThatCreates.buildARobot();
	}
	/**
	 * check if the robot is class destructor
	 * @param robot
	 * @return
	 */
	private boolean comproveDestructorv2(MarkJavav2 robot) {
		if (robot instanceof Destructorv2) {
			return true;
		} else
			return false;
	}
	/**
	 * check if the robot is class replicant
	 * @param robot
	 * @return
	 */
	private boolean comproveReplicantv2(MarkJavav2 robot) {
		if (robot instanceof Replicantv2) {
			return true;
		} else
			return false;
	}
	/**
	 * check if the robot is class mecanic
	 * @param robot
	 * @return
	 */
	private boolean comproveMecanicv2(MarkJavav2 robot) {
		if (robot instanceof Mecanicv2) {
			return true;
		} else
			return false;
	}
	/**
	 * this method passes the turns you pass through the parameter
	 * @param numTurns
	 */
	private void doTurns(int numTurns) {
		for (int i = 0; i < numTurns; i++) {
			turn();
			showEnergy();
		}
	}
	/**
	 * This method shows the energy of all the robots in the array
	 */
	private void showEnergy() {
		System.out.println("----------");
		for (int f = 0; f < SIZE; f++) {
			for (int c = 0; c < SIZE; c++) {
				if (robots[f][c] == null) {
					System.out.print("- ");
				} else {
					if (comproveDestructorv2(robots[f][c])) {
						System.out.print(robots[f][c].getEnergy()+ " ");
					}
					if (comproveReplicantv2(robots[f][c])) {
						System.out.print(robots[f][c].getEnergy()+ " ");

					}
					if (comproveMecanicv2(robots[f][c])) {
						System.out.print(robots[f][c].getEnergy()+ " ");
					}
				}
			}
			System.out.println();
		}
		System.out.println("----------");
	}
}
