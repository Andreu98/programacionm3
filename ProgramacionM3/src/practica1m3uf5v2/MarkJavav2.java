package practica1m3uf5v2;

import java.util.Random;

public class MarkJavav2 {

	Random random = new Random();
	private int energy=10;
	private boolean hasmove=true;
	
	public void setHasMove(boolean hasmove) {
		this.hasmove=hasmove;
	}
	/**
	 * gets the energy of the robot
	 * @return
	 */
	public int getEnergy() {
		return this.energy;
	}
	/**
	 * sets de energy of the robot to 0
	 */
	public void energyWasted() {
		this.energy=0;
	}
	/**
	 * recharge the energy of the robot to 10
	 */
	public void rechargeEnergy() {
		this.energy=10;
	}
	/**
	 * rest the energyWasted from the robot energy, if the energy is less than 0, throws an IllegalArgument exception
	 * @param energyWasted
	 * @throws IllegalArgumentException
	 */
	protected void energyWasted(int energyWasted) throws IllegalArgumentException{
		try {
			if (this.energy-energyWasted<0) {
				throw new IllegalArgumentException("La energia no puede ser menor que 0");
			}else this.energy= this.energy-energyWasted;
			
		} catch (IllegalArgumentException e) {
			System.err.println(e);// tambi�n podr�a poner e.getMessage()
		}
	}
}
