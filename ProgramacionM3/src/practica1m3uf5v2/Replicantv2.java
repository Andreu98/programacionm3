package practica1m3uf5v2;

public class Replicantv2 extends MarkJavav2 {
	
	MarkJavav2 robotToReplicate;
	private int tirada;

	/**
	 * if the energy is less than 7, it will not move, if the robot has 7 or more
	 * energy, a random from 1 to 10 is used to see if the robot finally moves ( the
	 * number must be 6 or less).
	 * 
	 * @return
	 */
	protected boolean decideToMove() {
		if (this.getEnergy() < 7) {
			return false;
		} else
			tirada = random.nextInt(10) + 1;
		System.out.println(tirada + " replicate");
		if (tirada <= 6) {
			return true;
		} else
			return false;
	}

	/**
	 * assign the robot found to the variable
	 * 
	 * @param anotherRobot
	 */
	protected void interact(MarkJavav2 anotherRobot) {
		robotToReplicate = anotherRobot;
	}

	/**
	 * build a robot with the variable @robotToReplicate , if the robot has not
	 * found a robot before, it build a random robot.
	 * 
	 * @return
	 */
	MarkJavav2 buildARobot() {
		if (robotToReplicate != null) {
			return robotToReplicate;
		} else {
			return randomRobot();
		}

	}

	/**
	 * generates a random robot
	 * 
	 * @return
	 */
	private MarkJavav2 randomRobot() {
		int num = random.nextInt(3);

		if (num == 0) {
			Destructorv2 destructor = new Destructorv2();
			return destructor;
		}
		if (num == 1) {
			Mecanicv2 mecanic = new Mecanicv2();
			return mecanic;
		}
		if (num == 2) {
			Replicantv2 replicant = new Replicantv2();
			return replicant;
		}
		return null;
	}

	public int getTirada() {
		return tirada;
	}
}
