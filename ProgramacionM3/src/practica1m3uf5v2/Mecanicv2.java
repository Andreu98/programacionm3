package practica1m3uf5v2;

public class Mecanicv2 extends MarkJavav2 {
	private int tirada;

	/**
	 * if the energy is less than 5, it will not move and the robot recharges its
	 * energy, if the robot has 5 or more energy, a random from 1 to 10 is used to
	 * see if the robot finally moves ( the number must be 6 or less).
	 * 
	 * @return
	 * 
	 */
	protected boolean decideToMove() {
		if (this.getEnergy() < 5) {
			this.rechargeEnergy();
			return false;
		} else
			tirada = random.nextInt(10) + 1;
		System.out.println(tirada + " mecanic");
		if (tirada <= 6) {
			return true;
		} else
			return false;
	}

	/**
	 * if this robot have an energy greather than 4, calls the method that
	 * rechargues the battery of the other robot, in the process, this robot will
	 * lose 1 point of energy
	 * 
	 * @param anotherRobot
	 */
	protected void interact(MarkJavav2 anotherRobot) {
		if (this.getEnergy() > 4) {
			anotherRobot.rechargeEnergy();
			this.energyWasted(1);
		}
	}

	public int getTirada() {
		return tirada;
	}
}
