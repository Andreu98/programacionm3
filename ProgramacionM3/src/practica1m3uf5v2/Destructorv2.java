package practica1m3uf5v2;

public class Destructorv2 extends MarkJavav2 {
	private int tirada;

	/**
	 * if the energy is less than 5, it will not move, if the robot has 5 or more
	 * energy, a random from 1 to 10 is used to see if the robot finally moves ( the
	 * number must be 4 or less).
	 * 
	 * @return
	 */
	protected boolean decideToMove() {
		if (this.getEnergy() < 5) {
			return false;
		} else
			tirada = random.nextInt(10) + 1;
		System.out.println(tirada + " destructor");
		if (tirada <= 4) {
			return true;
		} else
			return false;
	}

	/**
	 * This method interacts with other robots, if the robot with which it interacts
	 * is class "Destructor", it has a 50% chance of success, if it is of any other
	 * type, it has 100%. the robot will always spend 3 energy points when attacking
	 * 
	 * @param anotherRobot
	 */
	protected void interact(MarkJavav2 anotherRobot) {
		if (this.getEnergy() < 5) {

		} else {
			if (anotherRobot.getClass().equals(this.getClass())) {
				System.out.println("Ambos son destructores");
				if (random.nextBoolean()) {
					anotherRobot.energyWasted();
					this.energyWasted(3);
				} else {
					this.energyWasted(3);
				}
			} else {

				anotherRobot.energyWasted();
				this.energyWasted(3);
			}
		}
	}

	public int getTirada() {
		return tirada;
	}

}
