package a;

import java.util.Deque;

public class Kamikaze extends Conductor implements Constants, Animador {

	private int impactes = 0;

	Kamikaze(String nom) {
		super(nom);
	}

	@Override
	public void colisio(Deque<Conductor> boxes) {
		if(meMuero()) {
			boxes.addFirst(this);
		}
	}

	@Override
	public void repostar(Deque<Conductor> boxes) {
		this.sumAturades();
		this.setEstat(ConductorEstat.BOXES);
		boxes.addFirst(this);
	}

	private boolean meMuero() {
		this.impactes++;
		if (this.impactes == IMPACTES) {
			this.setEstat(ConductorEstat.OUT);
		}
		else  {
			this.setEstat(ConductorEstat.BOXES);
			return true;
		}
		return false;
	}
}
