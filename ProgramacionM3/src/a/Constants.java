package a;

public interface Constants {
	int GASOLINA = 6;
	int ATURADES = 6;
	int TIRADA_COMPETIDOR = 12;
	int TIRADA_ANIMADOR = 8;
	int IMPACTES = 2;
}
