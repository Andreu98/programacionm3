package a;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

public class Cursa implements Constants {
	private static final Random random = new Random();

	private int totalCaselles;
	private List<Conductor> conductors;
	private List<Conductor> boxes;
	//conductors �s la llista de conductors
	//boxes �s la cua de conductors amb problemes

	
	
	public void torn() {
		int k = 0;
		int pos;
		Conductor pil = null;

		for (Conductor c : conductors) {
			if (c.getEstat() == ConductorEstat.GO) {
				if (c instanceof Competidor)
					k = random.nextInt(TIRADA_COMPETIDOR) + 1;
				if (c instanceof Animador)
					k = random.nextInt(TIRADA_ANIMADOR) + 1;

				pos = c.avan�a(k);
				if (pos <= totalCaselles) {

					// COMPLETAR !!!
					// cal saber si impacta amb un altre conductor
					// imagina que �s pil el conductor impactat

					if (pil != null) {
						pil.colisio(boxes);
					}
					c.gastaGasolina();
					if (c.getGasolina() == 0)
						c.repostar(boxes);
				}
			}
		}

		this.gestioBoxes();
	}

	public boolean acabada() {
	}

	public void situacio() {
	}
}
