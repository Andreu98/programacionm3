package a;

import java.util.Arrays;
import java.util.Deque;

public abstract class Conductor implements Constants, Comparable<Conductor> {

	private String nom;
	private int casella;
	private int gasolina;
	private ConductorEstat estat;
	private int aturades;

	Conductor(String nom) {
		this.nom = nom;
		casella = 0;
		estat = ConductorEstat.GO;
		aturades = 0;
		gasolina = GASOLINA;
	}

	public String getNom() {
		return nom;
	}

	public void setGasolina(int gasolina) {
		this.gasolina += gasolina;
		if (this.gasolina > GASOLINA)
			this.gasolina = GASOLINA;
	}

	public int getGasolina() {
		return gasolina;
	}

	public void gastaGasolina() {
		gasolina--;
	}

	public void setCasella(int casella) {
		this.casella = casella;
	}

	public int getCasella() {
		return (casella);
	}

	public void aturar() {
		this.aturades++;
	}

	public int getAturades() {
		return (aturades);
	}
	public void sumAturades() {
		this.aturades++;
	}

	public int avan�a(int av) {
		casella = casella + av;
		return casella;
	}

	public void setEstat(ConductorEstat ce) {
		this.estat = ce;
	}

	public ConductorEstat getEstat() {
		return estat;
	}

	public abstract void colisio(Deque<Conductor> boxes);

	public abstract void repostar(Deque<Conductor> boxes);

	@Override
	public String toString() {
		return "Conductor [nom=" + nom + ", casella=" + casella + ", gasolina=" + gasolina + ", estat=" + estat
				+ ", aturades=" + aturades + "]";
	}

	@Override
	public int compareTo(Conductor other) {
		// alguno eliminao
		if (this.getEstat() == ConductorEstat.OUT && other.getEstat() != ConductorEstat.OUT) {
			return -1;
		}
		if (this.getEstat() != ConductorEstat.OUT && other.getEstat() == ConductorEstat.OUT) {
			return 1;
		}

		// casillas
		if (this.getCasella() > other.getCasella()) {
			return 1;
		}
		if (this.getCasella() < other.getCasella()) {
			return -1;
		}
		// misma casilla
		if (this.getAturades() < other.getAturades()) {
			return 1;
		}
		if (this.getAturades() > other.getAturades()) {
			return -1;
		}
		//mismas paradas
		return this.getNom().compareTo(other.getNom());

	}

}
