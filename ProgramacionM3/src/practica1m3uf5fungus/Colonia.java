package practica1m3uf5fungus;

public class Colonia implements Comparable<Colonia> {

	private String nom;
	private int poblacio;

	/**
	 * @param nom
	 */
	public Colonia(String nom) {
		this.nom = nom;
	}

	public void augmentaPoblacio() {
		this.poblacio += 1;
	}

	public void disminueixPoblacio() {
		this.poblacio -= 1;
	}

	/**
	 * Si esta colonia es mas grande que la otra, devuelve 1, en caso contrario, -1,
	 * si son iguales devuelve 0
	 */
	@Override
	public int compareTo(Colonia anotherColonia) throws ClassCastException {
		if (anotherColonia instanceof Colonia) {
			if (this.poblacio > ((Colonia) anotherColonia).poblacio) {
				return 1;
			}
			if (this.poblacio < ((Colonia) anotherColonia).poblacio) {
				return -1;
			} else
				return 0;
		} else
			throw new ClassCastException("El elemento a comparar no es una Colonia");
	}

	@Override
	public String toString() {
		return "Colonia [nom=" + nom + ", poblacio=" + poblacio + "]";
	}

}
