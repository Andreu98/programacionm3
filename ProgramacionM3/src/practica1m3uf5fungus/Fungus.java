package practica1m3uf5fungus;

public abstract class Fungus {

	Colonia colonia;
	int fila;
	int col;
	/**
	 * @param colonia
	 */
	public Fungus(Colonia colonia) {
		this.colonia = colonia;
	}
	public abstract char getChar();
	
	public abstract void creix(Colonia colonia);
	
	public  boolean posa (Colonia colonia, int fila, int col) {
		return true;
	}
	
}
